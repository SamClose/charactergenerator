﻿using CharacterGenerator.Constants;
using CharacterGenerator.Exceptions;
using CharacterGenerator.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Models
{
    public abstract class CharacterRace : BaseModel
    {
        public virtual Dictionary<string, CharacterRace> Subraces { get; set; }

        public abstract string Name { get; }
        public abstract int CONMod { get; protected set; }
        public abstract int STRMod { get; protected set; }
        public abstract int DEXMod { get; protected set; }
        public abstract int INTMod { get; protected set; }
        public abstract int WISMod { get; protected set; }
        public abstract int CHAMod { get; protected set; }

        public abstract int Speed { get; protected set; }
        //additional racial skills
    }

    #region Primary Races
    public class Aarakocra : CharacterRace
    {
        public override string Name => Race.Aarakocra;
        public override int CONMod { get; protected set; } = 0;
        public override int STRMod { get; protected set; } = 0;
        public override int DEXMod { get; protected set; } = 2;
        public override int INTMod { get; protected set; } = 0;
        public override int WISMod { get; protected set; } = 1;
        public override int CHAMod { get; protected set; } = 0;

        public override int Speed { get; protected set; } = 25;
    }
    public class Dragonborn : CharacterRace
    {
        public override string Name => Race.Dragonborn;
        public override int CONMod { get; protected set; } = 0;
        public override int STRMod { get; protected set; } = 2;
        public override int DEXMod { get; protected set; } = 0;
        public override int INTMod { get; protected set; } = 0;
        public override int WISMod { get; protected set; } = 0;
        public override int CHAMod { get; protected set; } = 1;
        public override int Speed { get; protected set; } = 30;
    }
    public class Dwarf : CharacterRace
    {
        public override string Name => Race.Dwarf;
        public override int CONMod { get; protected set; } = 2;
        public override int STRMod { get; protected set; } = 0;
        public override int DEXMod { get; protected set; } = 0;
        public override int INTMod { get; protected set; } = 0;
        public override int WISMod { get; protected set; } = 0;
        public override int CHAMod { get; protected set; } = 0;
        public override int Speed { get; protected set; } = 25;

    }
    public class Elf : CharacterRace
    {
        public override string Name => Race.Elf;
        public override int CONMod { get; protected set; } = 0;
        public override int STRMod { get; protected set; } = 0;
        public override int DEXMod { get; protected set; } = 2;
        public override int INTMod { get; protected set; } = 0;
        public override int WISMod { get; protected set; } = 0;
        public override int CHAMod { get; protected set; } = 0;
        public override int Speed { get; protected set; } = 30;

    }
    public class Genasi : CharacterRace
    {
        public override string Name => Race.Genasi;
        public override int CONMod { get; protected set; } = 2;
        public override int STRMod { get; protected set; } = 0;
        public override int DEXMod { get; protected set; } = 0;
        public override int INTMod { get; protected set; } = 0;
        public override int WISMod { get; protected set; } = 0;
        public override int CHAMod { get; protected set; } = 0;
        public override int Speed { get; protected set; } = 30;

    }
    public class Gnome : CharacterRace
    {
        public override string Name => Race.Gnome;
        public override int CONMod { get; protected set; } = 0;
        public override int STRMod { get; protected set; } = 0;
        public override int DEXMod { get; protected set; } = 0;
        public override int INTMod { get; protected set; } = 2;
        public override int WISMod { get; protected set; } = 0;
        public override int CHAMod { get; protected set; } = 0;
        public override int Speed { get; protected set; } = 25;

    }
    public class Goliath : CharacterRace
    {
        public override string Name => Race.Goliath;
        public override int CONMod { get; protected set; } = 1;
        public override int STRMod { get; protected set; } = 2;
        public override int DEXMod { get; protected set; } = 0;
        public override int INTMod { get; protected set; } = 0;
        public override int WISMod { get; protected set; } = 0;
        public override int CHAMod { get; protected set; } = 0;
        public override int Speed { get; protected set; } = 30;

    }
    public class HalfElf : CharacterRace
    {
        public HalfElf()
        {
            _IncreaseRandomAbilityScore();
            _IncreaseRandomAbilityScore();
        }

        public override string Name => Race.HalfElf;
        public override int CONMod { get; protected set; } = 0;
        public override int STRMod { get; protected set; } = 0;
        public override int DEXMod { get; protected set; } = 0;
        public override int INTMod { get; protected set; } = 0;
        public override int WISMod { get; protected set; } = 0;
        public override int CHAMod { get; protected set; } = 2;
        //TODO: Implement choice of ability scores
        //mvp: randomly pick them

        public override int Speed { get; protected set; } = 30;

        public void _IncreaseRandomAbilityScore()
        {
            var abilityName = CharacterHelper.PickAbility();
            _IncreaseAbilityScore(abilityName);
        }

        private void _IncreaseAbilityScore(string abilityName)
        {
            switch (abilityName)
            {
                case (Ability.Strength):
                    STRMod += 1;
                    break;
                case (Ability.Constitution):
                    CONMod += 1;
                    break;
                case (Ability.Dexterity):
                    DEXMod += 1;
                    break;
                case (Ability.Wisdom):
                    WISMod += 1;
                    break;
                case (Ability.Intelligence):
                    INTMod += 1;
                    break;
                case (Ability.Charisma):
                    CHAMod += 1;
                    break;
                default:
                    throw new StringNotFoundException($"Unable to identify ability name string: {abilityName}");
            }
        }
    }
    public class HalfOrc : CharacterRace
    {
        public override string Name => Race.HalfOrc;
        public override int CONMod { get; protected set; } = 1;
        public override int STRMod { get; protected set; } = 2;
        public override int DEXMod { get; protected set; } = 0;
        public override int INTMod { get; protected set; } = 0;
        public override int WISMod { get; protected set; } = 0;
        public override int CHAMod { get; protected set; } = 0;
        public override int Speed { get; protected set; } = 30;

    }
    public class Halfling : CharacterRace
    {
        public override string Name => Race.Halfling;
        public override int CONMod { get; protected set; } = 0;
        public override int STRMod { get; protected set; } = 0;
        public override int DEXMod { get; protected set; } = 2;
        public override int INTMod { get; protected set; } = 0;
        public override int WISMod { get; protected set; } = 0;
        public override int CHAMod { get; protected set; } = 0;
        public override int Speed { get; protected set; } = 25;

    }
    public class Human : CharacterRace
    {
        public override string Name => Race.Human;
        public override int CONMod { get; protected set; } = 1;
        public override int STRMod { get; protected set; } = 1;
        public override int DEXMod { get; protected set; } = 1;
        public override int INTMod { get; protected set; } = 1;
        public override int WISMod { get; protected set; } = 1;
        public override int CHAMod { get; protected set; } = 1;
        public override int Speed { get; protected set; } = 30;

    }
    public class Tiefling : CharacterRace
    {
        public override string Name => Race.Tiefling;
        public override int CONMod { get; protected set; } = 0;
        public override int STRMod { get; protected set; } = 0;
        public override int DEXMod { get; protected set; } = 0;
        public override int INTMod { get; protected set; } = 1;
        public override int WISMod { get; protected set; } = 0;
        public override int CHAMod { get; protected set; } = 2;
        public override int Speed { get; protected set; } = 30;

    }
    #endregion
    #region Sub-races
    public class HillDwarf : Dwarf
    {
        public override string Name => Race.HillDwarf;
        public override int CONMod { get; protected set; } = 0 + 2;
        public override int STRMod { get; protected set; } = 0 + 0;
        public override int DEXMod { get; protected set; } = 0 + 0;
        public override int INTMod { get; protected set; } = 0 + 0;
        public override int WISMod { get; protected set; } = 1 + 0;
        public override int CHAMod { get; protected set; } = 0 + 0;
    }
    public class MountainDwarf : Dwarf
    {
        public override string Name => Race.MountainDwarf;
        public override int CONMod { get; protected set; } = 2 + 2;
        public override int STRMod { get; protected set; } = 2 + 0;
        public override int DEXMod { get; protected set; } = 0 + 0;
        public override int INTMod { get; protected set; } = 0 + 0;
        public override int WISMod { get; protected set; } = 0 + 0;
        public override int CHAMod { get; protected set; } = 0 + 0;
    }

    public class EladrinElf : Elf
    {
        public override string Name => Race.EladrinElf;
        public override int CONMod { get; protected set; } = 0 + 0;
        public override int STRMod { get; protected set; } = 0 + 0;
        public override int DEXMod { get; protected set; } = 0 + 2;
        public override int INTMod { get; protected set; } = 1 + 0;
        public override int WISMod { get; protected set; } = 0 + 0;
        public override int CHAMod { get; protected set; } = 0 + 0;
    }
    public class HighElf : Elf
    {
        public override string Name => Race.HighElf;
        public override int CONMod { get; protected set; } = 0 + 0;
        public override int STRMod { get; protected set; } = 0 + 0;
        public override int DEXMod { get; protected set; } = 0 + 2;
        public override int INTMod { get; protected set; } = 1 + 0;
        public override int WISMod { get; protected set; } = 0 + 0;
        public override int CHAMod { get; protected set; } = 0 + 0;
    }
    public class WoodElf : Elf
    {
        public override string Name => Race.WoodElf;
        public override int CONMod { get; protected set; } = 0 + 0;
        public override int STRMod { get; protected set; } = 0 + 0;
        public override int DEXMod { get; protected set; } = 0 + 2;
        public override int INTMod { get; protected set; } = 0 + 0;
        public override int WISMod { get; protected set; } = 1 + 0;
        public override int CHAMod { get; protected set; } = 0 + 0;
    }
    public class AirGenasi : Genasi
    {
        public override string Name => Race.AirGenasi;
        public override int CONMod { get; protected set; } = 0 + 2;
        public override int STRMod { get; protected set; } = 0 + 0;
        public override int DEXMod { get; protected set; } = 1 + 0;
        public override int INTMod { get; protected set; } = 0 + 0;
        public override int WISMod { get; protected set; } = 0 + 0;
        public override int CHAMod { get; protected set; } = 0 + 0;
    }
    public class EarthGenasi : Genasi
    {
        public override string Name => Race.EarthGenasi;
        public override int CONMod { get; protected set; } = 0 + 2;
        public override int STRMod { get; protected set; } = 1 + 0;
        public override int DEXMod { get; protected set; } = 0 + 0;
        public override int INTMod { get; protected set; } = 0 + 0;
        public override int WISMod { get; protected set; } = 0 + 0;
        public override int CHAMod { get; protected set; } = 0 + 0;
    }
    public class FireGenasi : Genasi
    {
        public override string Name => Race.FireGenasi;
        public override int CONMod { get; protected set; } = 0 + 2;
        public override int STRMod { get; protected set; } = 0 + 0;
        public override int DEXMod { get; protected set; } = 0 + 0;
        public override int INTMod { get; protected set; } = 1 + 0;
        public override int WISMod { get; protected set; } = 0 + 0;
        public override int CHAMod { get; protected set; } = 0 + 0;
    }
    public class WaterGenasi : Genasi
    {
        public override string Name => Race.WaterGenasi;
        public override int CONMod { get; protected set; } = 0 + 2;
        public override int STRMod { get; protected set; } = 0 + 0;
        public override int DEXMod { get; protected set; } = 0 + 0;
        public override int INTMod { get; protected set; } = 0 + 0;
        public override int WISMod { get; protected set; } = 1 + 0;
        public override int CHAMod { get; protected set; } = 0 + 0;
    }
    public class LightfootHalfling : Halfling
    {
        public override string Name => Race.LightfootHalfling;
        public override int CONMod { get; protected set; } = 0 + 0;
        public override int STRMod { get; protected set; } = 0 + 0;
        public override int DEXMod { get; protected set; } = 0 + 2;
        public override int INTMod { get; protected set; } = 0 + 0;
        public override int WISMod { get; protected set; } = 0 + 0;
        public override int CHAMod { get; protected set; } = 1 + 0;
    }
    public class StoutHalfling : Halfling
    {
        public override string Name => Race.StoutHalfling;
        public override int CONMod { get; protected set; } = 1 + 0;
        public override int STRMod { get; protected set; } = 0 + 0;
        public override int DEXMod { get; protected set; } = 0 + 2;
        public override int INTMod { get; protected set; } = 0 + 0;
        public override int WISMod { get; protected set; } = 0 + 0;
        public override int CHAMod { get; protected set; } = 0 + 0;
    }
    #endregion
}
