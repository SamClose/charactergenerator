﻿using CharacterGenerator.Models.Interfaces;
using CharacterGenerator.Utilities;
using System.Collections.Generic;

namespace CharacterGenerator.Models
{
    public class Character : BaseModel, ICharacter
    {
        public Character()
        {
            SetupAbilities();
            SetupRace();
            SetupClass();
            SetupBackground();

            SetupAbilityModifiers();
            SetupHealth();

            SetupSkillProficiencies();
        }

        private CharacterRace Race { get; set; }
        public string RaceName => Race.Name;
        public CharacterClass Class { get; private set; }
        public string ClassName => Class.Name;
        public CharacterBackground Background { get; private set; }
        public string BackgroundName => Background.Name;

        #region Abilities
        public int? Strength { get; private set; }
        public int? Constitution { get; private set; }
        public int? Dexterity { get; private set; }
        public int? Wisdom { get; private set; }
        public int? Intelligence { get; private set; }
        public int? Charisma { get; private set; }
        #endregion

        #region Ability Modifiers
        public int Mod_STR { get; private set; }
        public int Mod_CON { get; private set; }
        public int Mod_DEX { get; private set; }
        public int Mod_WIS { get; private set; }
        public int Mod_INT { get; private set; }
        public int Mod_CHA { get; private set; }
        #endregion


        public int Speed { get; private set; }
        //hit points
        public int MaxHitPoints { get; private set; }
        public int CurrentHitPoints { get; private set; }
        public int TemporaryHitPoints { get; private set; }

        //proficiencies
        //TODO: Improve this section
        private HashSet<string> SkillProficiencies { get; set; } = new HashSet<string>();
        public string SkillProficienciesText { get; private set; }

        private string name;
        public string Name
        {
            get => name;
            set { SetProperty(ref name, value); }
        }

        private string notes;
        public string Notes
        {
            get => notes;
            set { SetProperty(ref notes, value); }
        }

        //equipment
        //armor class

        #region Setup
        private void SetupSkillProficiencies()
        {
            //TODO: Include background
            foreach (var skill in Class.SkillProficiencies)
            {
                SkillProficiencies.Add(skill);
            }
            foreach (var skill in Background.SkillProficiencies)
            {
                SkillProficiencies.Add(skill);
            }
            foreach (var skill in SkillProficiencies)
            {
                SkillProficienciesText += $"{skill}. ";
            }
        }

        private void SetupAbilities()
        {
            Strength = GenerateStat();
            Constitution = GenerateStat();
            Dexterity = GenerateStat();
            Wisdom = GenerateStat();
            Intelligence = GenerateStat();
            Charisma = GenerateStat();
        }

        private void SetupAbilityModifiers()
        {
            Mod_STR = ModifierFromAbility(Strength);
            Mod_CON = ModifierFromAbility(Constitution);
            Mod_DEX = ModifierFromAbility(Dexterity);
            Mod_WIS = ModifierFromAbility(Wisdom);
            Mod_INT = ModifierFromAbility(Intelligence);
            Mod_CHA = ModifierFromAbility(Charisma);
        }

        private void SetupHealth()
        {
            MaxHitPoints = Class.HitDie + Mod_CON;
            CurrentHitPoints = MaxHitPoints;
            TemporaryHitPoints = 0;
        }

        private void SetupBackground()
        {
            Background = CharacterHelper.PickBackground();
        }

        private void SetupClass()
        {
            Class = CharacterHelper.PickClass();
        }

        private void SetupRace()
        {
            Race = CharacterHelper.PickRace();
            Strength += Race.STRMod;
            Constitution += Race.CONMod;
            Dexterity += Race.DEXMod;
            Intelligence += Race.INTMod;
            Wisdom += Race.WISMod;
            Charisma += Race.CHAMod;

            Speed = Race.Speed;
        }
        #endregion

        private int GenerateStat()
        {
            var items = new List<int>();
            
            for(var i = 0; i < 4; i++)
            {
                items.Add(CharacterHelper.RollDie(6));
            }
            items.Sort();
            return items[1] + items[2] + items[3];
        }

        private int ModifierFromAbility(int? ability)
        {
            if(!ability.HasValue) { return 0; }

            return (ability.Value - 10) / 2;
        }
    }
}
