﻿using CharacterGenerator.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Models
{
    public abstract class CharacterBackground : BaseModel
    {
        public abstract string Name { get; }
        public abstract IList<string> SkillProficiencies { get; }
    }

    public class Acolyte : CharacterBackground
    {
        public override string Name => Background.Acolyte;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Insight,
            Skill.Religion,
        };
    }
    public class Charlatan : CharacterBackground
    {
        public override string Name => Background.Charlatan;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Deception,
            Skill.SleightOfHand,
        };
    }
    public class Criminal : CharacterBackground
    {
        public override string Name => Background.Criminal;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Deception,
            Skill.Stealth,
        };
    }
    public class Entertainer : CharacterBackground
    {
        public override string Name => Background.Entertainer;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Acrobatics,
            Skill.Performance,
        };
    }
    public class FolkHero : CharacterBackground
    {
        public override string Name => Background.FolkHero;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.AnimalHandling,
            Skill.Survival,
        };
    }
    public class Gladiator : CharacterBackground
    {
        public override string Name => Background.Gladiator;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Acrobatics,
            Skill.Performance,
        };
    }
    public class GuildArtisan : CharacterBackground
    {
        public override string Name => Background.GuildArtisan;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Insight,
            Skill.Persuasion,
        };
    }
    public class GuildMerchant : CharacterBackground
    {
        public override string Name => Background.GuildMerchant;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Insight,
            Skill.Persuasion,
        };
    }
    public class Hermit : CharacterBackground
    {
        public override string Name => Background.Hermit;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Medicine,
            Skill.Religion,
        };
    }
    public class Knight : CharacterBackground
    {
        public override string Name => Background.Knight;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.History,
            Skill.Persuasion,
        };
    }
    public class Noble : CharacterBackground
    {
        public override string Name => Background.Noble;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.History,
            Skill.Persuasion,
        };
    }
    public class Outlander : CharacterBackground
    {
        public override string Name => Background.Outlander;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Athletics,
            Skill.Survival,
        };
    }
    public class Pirate : CharacterBackground
    {
        public override string Name => Background.Pirate;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Athletics,
            Skill.Perception,
        };
    }
    public class Sage : CharacterBackground
    {
        public override string Name => Background.Sage;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Arcana,
            Skill.History,
        };
    }
    public class Sailor : CharacterBackground
    {
        public override string Name => Background.Sailor;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Athletics,
            Skill.Perception,
        };
    }
    public class Soldier : CharacterBackground
    {
        public override string Name => Background.Soldier;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Athletics,
            Skill.Intimidation,
        };
    }
    public class Spy : CharacterBackground
    {
        public override string Name => Background.Spy;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.Deception,
            Skill.Stealth,
        };
    }
    public class Urchin : CharacterBackground
    {
        public override string Name => Background.Urchin;
        public override IList<string> SkillProficiencies { get; } = new List<string>
        {
            Skill.SleightOfHand,
            Skill.Stealth,
        };
    }
}
