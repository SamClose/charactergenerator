﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Models.Interfaces
{
    public interface ICharacter
    {
        string RaceName { get; }
        string ClassName { get; }
        string BackgroundName { get; }
        CharacterBackground Background { get; }
        int? Strength { get; }
        int? Constitution { get; }
        int? Dexterity { get; }
        int? Wisdom { get; }
        int? Intelligence { get; }
        int? Charisma { get; }
        int Speed { get; }
        int MaxHitPoints { get; }
        int CurrentHitPoints { get; }
        string SkillProficienciesText { get; }
        string Name { get; set; }
        string Notes { get; set; }
    }
}
