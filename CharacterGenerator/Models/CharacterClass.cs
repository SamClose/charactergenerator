﻿using CharacterGenerator.Constants;
using CharacterGenerator.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Models
{
    public abstract class CharacterClass : BaseModel
    {
        protected static Random _rand = new Random();
        protected CharacterClass()
        {
            SelectSkillProficiencies();
        }
        public abstract string Name { get; }
        public abstract int HitDie { get; }
        public abstract int SkillProficiencyCount { get; }
        //proficiencies
        public abstract List<string> PotentialSkillProficiencies { get; }
        public HashSet<string> SkillProficiencies { get; } = new HashSet<string>();
        protected void SelectSkillProficiencies()
        {
            var unselectedSkills = new List<string>(PotentialSkillProficiencies);
            for(var i = 0; i < SkillProficiencyCount; i++)
            {
                var skill = unselectedSkills.GetRandomItem();
                unselectedSkills.Remove(skill);
                SkillProficiencies.Add(skill);
            }
        }
        
        //equipment
        //armor class
    }

    public class Barbarian : CharacterClass
    {
        public Barbarian() : base() { }

        public override string Name => GameClass.Barbarian;
        public override int HitDie => 12;
        public override int SkillProficiencyCount => 2;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        { 
            Skill.AnimalHandling,
            Skill.Athletics,
            Skill.Intimidation,
            Skill.Nature,
            Skill.Perception,
            Skill.Survival
        };

    }
    public class Bard : CharacterClass
    {
        public Bard() : base() { }

        public override string Name => GameClass.Bard;
        public override int HitDie => 8;
        public override int SkillProficiencyCount => 3;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.Acrobatics,
            Skill.AnimalHandling,
            Skill.Arcana,
            Skill.Athletics,
            Skill.Deception,
            Skill.History,
            Skill.Insight,
            Skill.Intimidation,
            Skill.Investigation,
            Skill.Medicine,
            Skill.Nature,
            Skill.Perception,
            Skill.Performance,
            Skill.Persuasion,
            Skill.Religion,
            Skill.SleightOfHand,
            Skill.Stealth,
            Skill.Survival
        };
    }
    public class Cleric : CharacterClass
    {
        public Cleric() : base() { }

        public override string Name => GameClass.Cleric;
        public override int HitDie => 8;
        public override int SkillProficiencyCount => 2;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.History,
            Skill.Insight,
            Skill.Medicine,
            Skill.Persuasion,
            Skill.Religion,
        };
    }
    public class Druid : CharacterClass
    {
        public Druid() : base() { }

        public override string Name => GameClass.Druid;
        public override int HitDie => 8;
        public override int SkillProficiencyCount => 2;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.AnimalHandling,
            Skill.Arcana,
            Skill.Insight,
            Skill.Medicine,
            Skill.Nature,
            Skill.Perception,
            Skill.Religion,
            Skill.Survival
        };
    }
    public class Fighter : CharacterClass
    {
        public Fighter() : base() { }

        public override string Name => GameClass.Fighter;
        public override int HitDie => 10;
        public override int SkillProficiencyCount => 2;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.Acrobatics,
            Skill.AnimalHandling,
            Skill.Athletics,
            Skill.History,
            Skill.Insight,
            Skill.Intimidation,
            Skill.Perception,
            Skill.Survival
        };
    }
    public class Monk : CharacterClass
    {
        public Monk() : base() { }

        public override string Name => GameClass.Monk;
        public override int HitDie => 8;
        public override int SkillProficiencyCount => 2;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.Acrobatics,
            Skill.Athletics,
            Skill.History,
            Skill.Insight,
            Skill.Religion,
            Skill.Stealth,
        };
    }
    public class Paladin : CharacterClass
    {
        public Paladin() : base() { }

        public override string Name => GameClass.Paladin;
        public override int HitDie => 10;
        public override int SkillProficiencyCount => 2;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.Athletics,
            Skill.Insight,
            Skill.Intimidation,
            Skill.Medicine,
            Skill.Persuasion,
            Skill.Religion,
        };
    }
    public class Ranger : CharacterClass
    {
        public Ranger() : base() { }

        public override string Name => GameClass.Ranger;
        public override int HitDie => 10;
        public override int SkillProficiencyCount => 3;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.AnimalHandling,
            Skill.Athletics,
            Skill.Insight,
            Skill.Investigation,
            Skill.Nature,
            Skill.Perception,
            Skill.Stealth,
            Skill.Survival
        };
    }
    public class Rogue : CharacterClass
    {
        public Rogue() : base() { }

        public override string Name => GameClass.Rogue;
        public override int HitDie => 8;
        public override int SkillProficiencyCount => 4;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.Acrobatics,
            Skill.Athletics,
            Skill.Deception,
            Skill.Insight,
            Skill.Intimidation,
            Skill.Investigation,
            Skill.Perception,
            Skill.Performance,
            Skill.Persuasion,
            Skill.SleightOfHand,
            Skill.Stealth,
        };
    }
    public class Sorcerer : CharacterClass
    {
        public Sorcerer() : base() { }

        public override string Name => GameClass.Sorcerer;
        public override int HitDie => 6;
        public override int SkillProficiencyCount => 2;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.Arcana,
            Skill.Deception,
            Skill.Insight,
            Skill.Intimidation,
            Skill.Persuasion,
            Skill.Religion,
        };
    }
    public class Warlock : CharacterClass
    {
        public Warlock() : base() { }

        public override string Name => GameClass.Warlock;
        public override int HitDie => 8;
        public override int SkillProficiencyCount => 2;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.Arcana,
            Skill.Deception,
            Skill.History,
            Skill.Intimidation,
            Skill.Investigation,
            Skill.Nature,
            Skill.Religion,
        };
    }
    public class Wizard : CharacterClass
    {
        public Wizard() : base() { }

        public override string Name => GameClass.Wizard;
        public override int HitDie => 6;
        public override int SkillProficiencyCount => 2;
        public override List<string> PotentialSkillProficiencies { get; } = new List<string>()
        {
            Skill.Arcana,
            Skill.History,
            Skill.Insight,
            Skill.Investigation,
            Skill.Medicine,
            Skill.Religion,
        };
    }
}
