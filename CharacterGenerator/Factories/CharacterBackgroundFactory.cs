﻿using CharacterGenerator.Constants;
using CharacterGenerator.Exceptions;
using CharacterGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Factories
{
    public static class CharacterBackgroundFactory
    {
        public static CharacterBackground CreateBackground(string name)
        {
            switch (name)
            {
                case Background.Acolyte:
                    return new Acolyte();
                case Background.Charlatan:
                    return new Charlatan();
                case Background.Criminal:
                    return new Criminal();
                case Background.Entertainer:
                    return new Entertainer();
                case Background.FolkHero:
                    return new FolkHero();
                case Background.Gladiator:
                    return new Gladiator();
                case Background.GuildArtisan:
                    return new GuildArtisan();
                case Background.GuildMerchant:
                    return new GuildMerchant();
                case Background.Hermit:
                    return new Hermit();
                case Background.Knight:
                    return new Knight();
                case Background.Noble:
                    return new Noble();
                case Background.Outlander:
                    return new Outlander();
                case Background.Pirate:
                    return new Pirate();
                case Background.Sage:
                    return new Sage();
                case Background.Sailor:
                    return new Sailor();
                case Background.Soldier:
                    return new Soldier();
                case Background.Spy:
                    return new Spy();
                case Background.Urchin:
                    return new Urchin();
                default:
                    throw new StringNotFoundException($"Unable to identify background string: {name}");
            }
        }
    }
}
