﻿using CharacterGenerator.Models;
using CharacterGenerator.Models.Interfaces;

namespace CharacterGenerator.Factories
{
    public class CharacterFactory
    {
        public static ICharacter CreateCharacter()
        {
            return new Character();
        }
    }
}
