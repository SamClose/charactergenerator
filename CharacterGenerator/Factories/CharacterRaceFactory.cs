﻿using CharacterGenerator.Constants;
using CharacterGenerator.Exceptions;
using CharacterGenerator.Models;
using System;

namespace CharacterGenerator.Factories
{
    public static class CharacterRaceFactory
    {
        public static Random rand = new Random();

        public static CharacterRace CreateRace(string raceName)
        {
            switch (raceName)
            {
                case Race.Aarakocra:
                    return new Aarakocra();
                case Race.Dragonborn:
                    return new Dragonborn();
                case Race.Dwarf:
                    return ChooseDwarf();
                case Race.Elf:
                    return ChooseElf();
                case Race.Genasi:
                    return ChooseGenasi();
                case Race.Gnome:
                    return new Gnome();
                case Race.Goliath:
                    return new Goliath();
                case Race.HalfElf:
                    return new HalfElf();
                case Race.Halfling:
                    return ChooseHalfling();
                case Race.HalfOrc:
                    return new HalfOrc();
                case Race.Human:
                    return new Human();
                case Race.Tiefling:
                    return new Tiefling();
                default:
                    throw new StringNotFoundException($"Unable to identify race string: {raceName}");
            }
        }

        private static CharacterRace ChooseHalfling()
        {
            var randVal = rand.Next(0, 2);
            switch (randVal)
            {
                case 0:
                    return new LightfootHalfling();
                case 1:
                    return new StoutHalfling();
                default:
                    return null;
            }
        }

        private static CharacterRace ChooseGenasi()
        {
            var randVal = rand.Next(0, 4);
            switch (randVal)
            {
                case 0:
                    return new AirGenasi();
                case 1:
                    return new EarthGenasi();
                case 2:
                    return new FireGenasi();
                case 3:
                    return new WaterGenasi();
                default:
                    return null;
            }
        }

        private static CharacterRace ChooseElf()
        {
            var randVal = rand.Next(0, 3);
            switch (randVal)
            {
                case 0:
                    return new EladrinElf();
                case 1:
                    return new HighElf();
                case 2:
                    return new WoodElf();
                default:
                    return null;
            }
        }

        private static CharacterRace ChooseDwarf()
        {
            var randVal = rand.Next(0, 2);
            switch (randVal)
            {
                case 0:
                    return new HillDwarf();
                case 1:
                    return new MountainDwarf();
                default:
                    return null;
            }
        }
    }
}
