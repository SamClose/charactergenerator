﻿using CharacterGenerator.Constants;
using CharacterGenerator.Exceptions;
using CharacterGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Factories
{
    public static class CharacterClassFactory
    {
        public static CharacterClass CreateClass(string className)
        {
            var matchedClass = _MatchClass(className);

            return matchedClass;
        }

        private static CharacterClass _MatchClass(string className)
        {
            switch (className)
            {
                case GameClass.Barbarian:
                    return new Barbarian();
                case GameClass.Bard:
                    return new Bard();
                case GameClass.Cleric:
                    return new Cleric();
                case GameClass.Druid:
                    return new Druid();
                case GameClass.Fighter:
                    return new Fighter();
                case GameClass.Monk:
                    return new Monk();
                case GameClass.Paladin:
                    return new Paladin();
                case GameClass.Ranger:
                    return new Ranger();
                case GameClass.Rogue:
                    return new Rogue();
                case GameClass.Sorcerer:
                    return new Sorcerer();
                case GameClass.Warlock:
                    return new Warlock();
                case GameClass.Wizard:
                    return new Wizard();
                default:
                    throw new StringNotFoundException($"Unable to identify class string: {className}");
            }
        }
    }
}
