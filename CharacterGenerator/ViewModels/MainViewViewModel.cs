﻿using CharacterGenerator.Factories;
using CharacterGenerator.Models;
using CharacterGenerator.Models.Interfaces;
using CharacterGenerator.Services;
using CharacterGenerator.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CharacterGenerator.ViewModels
{
    public class MainViewViewModel : BaseModel
    {
        public MainViewViewModel(IDataStoreService dataStoreService)
        {
            this.dataStoreService = dataStoreService;

            Character = CharacterFactory.CreateCharacter();

            RefreshCommand = new RelayCommand(_ => _Refresh());
            SaveCharacterCommand = new RelayCommand(_ => _SaveCharacter(Character));
        }

        public ICommand RefreshCommand { get; }
        public ICommand SaveCharacterCommand { get; }

        private readonly IDataStoreService dataStoreService;

        private ICharacter _character;
        public ICharacter Character
        {
            get { return _character; }
            set 
            {
                if(SetProperty(ref _character, value))
                {
                    SetDisplayText();
                    ResetInputFields();
                }
            }
        }

        private string _displayText;
        public string DisplayText
        {
            get => _displayText;
            set { SetProperty(ref _displayText, value); }
        }

        private string _saveText = "Save";
        public string SaveText
        {
            get => _saveText;
            set { SetProperty(ref _saveText, value); }
        }

        private string _notes;
        public string Notes
        {
            get => _notes;
            set
            {
                if( SetProperty(ref _notes, value))
                {
                    Character.Notes = _notes;
                }
            }
        }

        private string _name;
        public string Name
        {
            get => _name;
            set 
            {
                if(SetProperty(ref _name, value))
                {
                    Character.Name = _name;
                }
            }
        }

        private void SetDisplayText()
        {
            //This works for displaying everything but becomes a problem if I want to edit fields
            //ex: re-roll a single field

            DisplayText = string.Empty;

            DisplayText += $"Race:        {Character.RaceName}\n";
            DisplayText += $"Class:       {Character.ClassName}\n";
            DisplayText += $"Background:  {Character.BackgroundName}\n";

            DisplayText += $"Strength:    {Character.Strength.ToString()}\n";
            DisplayText += $"Constitution:{Character.Constitution.ToString()}\n";
            DisplayText += $"Dexterity:   {Character.Dexterity.ToString()}\n";
            DisplayText += $"Wisdom:      {Character.Wisdom.ToString()}\n";
            DisplayText += $"Intelligence:{Character.Intelligence.ToString()}\n";
            DisplayText += $"Charisma:    {Character.Charisma.ToString()}\n";

            DisplayText += $"Speed:       {Character.Speed.ToString()}\n";
            DisplayText += $"Max Hit Points:       {Character.MaxHitPoints.ToString()}\n";
            DisplayText += $"Current Hit Points:       {Character.CurrentHitPoints.ToString()}\n";

            DisplayText += $"Skill Proficiencies: {Character.SkillProficienciesText}\n";
        }

        private void ResetInputFields()
        {
            Notes = Character?.Notes;
            Name = Character?.Name;
        }

        private void _Refresh()
        {
            Character = CharacterFactory.CreateCharacter();
        }


        private void _SaveCharacter(ICharacter character)
        {
            dataStoreService.SaveCharacter(character);

            SaveText = $"Save ({dataStoreService.SavedCharacters.Count})";
        }

    }
}
