﻿using CharacterGenerator.Models;
using CharacterGenerator.Models.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace CharacterGenerator.Services
{
    public class DataStoreService : IDataStoreService
    {
        private string saveDirectory = "C:\\Users\\Sam\\Desktop\\Projects\\InProgress\\CharacterGenerator";

        public ObservableCollection<ICharacter> SavedCharacters { get; } = new ObservableCollection<ICharacter>();

        public void SaveCharacter(ICharacter character)
        {
            SavedCharacters.Add(character);

            var serializedCharacter = JsonConvert.SerializeObject(character);
            var fileName = Guid.NewGuid();
            //write to file
            var destination = $"{saveDirectory}\\{fileName}";
            using (var fileStream = File.Create(destination))
            {
                byte[] info = new UTF8Encoding(true).GetBytes(serializedCharacter);
                fileStream.Write(info, 0, info.Length);
            }

            //here for testing
            var characters = LoadSavedCharacters();
        }

        public ICollection<ICharacter> LoadSavedCharacters()
        {
            var fileStrings = Directory.EnumerateFiles(saveDirectory);

            var characters = new List<ICharacter>();

            foreach(var fileName in fileStrings)
            {
                var character = ReadCharacter(fileName);
                characters.Add(character);
            }

            return characters;
        }


        private ICharacter ReadCharacter(string filePath)
        {
            //deserialize into a character
            var result = string.Empty;
            using (StreamReader sr = File.OpenText(filePath))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    //this is bullshit use a stringbuilder
                    result += s;
                }
            }

            var deserialized = JsonConvert.DeserializeObject<Character>(result);
            return deserialized;
        }
    }
}
