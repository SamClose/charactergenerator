﻿using CharacterGenerator.Models.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CharacterGenerator.Services
{
    public interface IDataStoreService
    {
        ObservableCollection<ICharacter> SavedCharacters { get; }

        void SaveCharacter(ICharacter character);

        ICollection<ICharacter> LoadSavedCharacters();
    }
}