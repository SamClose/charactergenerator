﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Exceptions
{
    public class StringNotFoundException : Exception
    {
        public StringNotFoundException(string message) : base(message)
        {
        }

        public StringNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
