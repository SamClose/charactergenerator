﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Extensions
{
    public static class ListExtensions
    {
        private static Random _rand = new Random();
        public static T GetRandomItem<T>(this IEnumerable<T> list)
        {
            if(list == null || list.Count() == 0) { return default; }

            var index = _rand.Next(0, list.Count());
            return list.ElementAt(index);
        }
    }
}
