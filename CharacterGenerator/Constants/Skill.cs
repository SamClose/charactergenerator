﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Constants
{
    public class Skill
    {
        public const string Acrobatics = "Acrobatics";
        public const string AnimalHandling = "Animal Handling";
        public const string Arcana = "Arcana";
        public const string Athletics = "Athletics";
        public const string Deception = "Deception";
        public const string History = "History";
        public const string Insight = "Insight";
        public const string Intimidation = "Intimidation";
        public const string Investigation = "Investigation";
        public const string Medicine = "Medicine";
        public const string Nature = "Nature";
        public const string Perception = "Perception";
        public const string Performance = "Performance";
        public const string Persuasion = "Persuasion";
        public const string Religion = "Religion";
        public const string SleightOfHand = "Sleight Of Hand";
        public const string Stealth = "Stealth";
        public const string Survival = "Survival";
    }
}
