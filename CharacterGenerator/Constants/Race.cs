﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Constants
{
    public class Race
    {
        //primary races
        public const string Aarakocra = "Aarakocra";
        public const string Dragonborn = "Dragonborn";
        public const string Dwarf = "Dwarf";
        public const string Elf = "Elf";
        public const string Genasi = "Genasi";
        public const string Gnome = "Gnome";
        public const string Goliath = "Goliath";
        public const string HalfElf = "Half-Elf";
        public const string HalfOrc = "Half-Orc";
        public const string Halfling = "Halfling";
        public const string Human = "Human";
        public const string Tiefling = "Tiefling";

        //subraces
        public const string HillDwarf = "Hill Dwarf";
        public const string MountainDwarf = "Mountain Dwarf";

        public const string EladrinElf = "Eladrin Elf";
        public const string HighElf = "High Elf";
        public const string WoodElf = "Wood Elf";

        public const string AirGenasi = "Air Genasi";
        public const string EarthGenasi = "Earth Genasi";
        public const string FireGenasi = "Fire Genasi";
        public const string WaterGenasi = "Water Genasi";

        public const string LightfootHalfling = "Lightfoot Halfling";
        public const string StoutHalfling = "Stout Halfling";
    }
}
