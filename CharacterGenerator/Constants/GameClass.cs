﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Constants
{
    public class GameClass
    {
        public const string Barbarian = "Barbarian";
        public const string Bard = "Bard";
        public const string Cleric = "Cleric";
        public const string Druid = "Druid";
        public const string Fighter = "Fighter";
        public const string Monk = "Monk";
        public const string Paladin = "Paladin";
        public const string Ranger = "Ranger";
        public const string Rogue = "Rogue";
        public const string Sorcerer = "Sorcerer";
        public const string Warlock = "Warlock";
        public const string Wizard = "Wizard";
    }
}
