﻿namespace CharacterGenerator.Constants
{
    public class Background
    {
        public const string Acolyte = "Acolyte";
        public const string Charlatan = "Charlatan";
        public const string Criminal = "Criminal";
        public const string Entertainer = "Entertainer";
        public const string FolkHero = "Folk Hero";
        public const string Gladiator = "Gladiator";
        public const string GuildArtisan = "Guild Artisan";
        public const string GuildMerchant = "Guild Merchant";
        public const string Hermit = "Hermit";
        public const string Knight = "Knight";
        public const string Noble = "Noble";
        public const string Outlander = "Outlander";
        public const string Pirate = "Pirate";
        public const string Sage = "Sage";
        public const string Sailor = "Sailor";
        public const string Soldier = "Soldier";
        public const string Spy = "Spy";
        public const string Urchin = "Urchin";
    }
}
