﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Constants
{
    public class Ability
    {
        public const string Strength = "Strength";
        public const string Constitution = "Constitution";
        public const string Dexterity = "Dexterity";
        public const string Wisdom = "Wisdom";
        public const string Intelligence = "Intelligence";
        public const string Charisma = "Charisma";
    }
}
