﻿using CharacterGenerator.Constants;
using CharacterGenerator.Extensions;
using CharacterGenerator.Factories;
using CharacterGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterGenerator.Utilities
{
    public static class CharacterHelper
    {
        private static Random _rand = new Random();

        private static readonly Dictionary<int, string> _abilities = new Dictionary<int, string>()
        {
            {0, Ability.Strength},
            {1, Ability.Constitution},
            {2, Ability.Dexterity},
            {3, Ability.Wisdom},
            {4, Ability.Intelligence},
            {5, Ability.Charisma},
        };

        private static readonly Dictionary<int, string> _backgrounds = new Dictionary<int, string>()
        {
            {0, Background.Acolyte},
            {1, Background.Charlatan},
            {2, Background.Criminal},
            {3, Background.Entertainer},
            {4, Background.FolkHero},
            {5, Background.Gladiator},
            {6, Background.GuildArtisan},
            {7, Background.GuildMerchant},
            {8, Background.Hermit},
            {9, Background.Knight},
            {10, Background.Noble},
            {11, Background.Outlander},
            {12, Background.Pirate},
            {13, Background.Sailor},
            {14, Background.Soldier},
            {15, Background.Spy},
            {16, Background.Urchin},
        };

        private static readonly Dictionary<int, string> _classes = new Dictionary<int, string>()
        {
            {0, GameClass.Barbarian },
            {1, GameClass.Bard },
            {2, GameClass.Cleric },
            {3, GameClass.Druid },
            {4, GameClass.Fighter },
            {5, GameClass.Monk },
            {6, GameClass.Paladin },
            {7, GameClass.Ranger },
            {8, GameClass.Rogue },
            {9, GameClass.Sorcerer },
            {10, GameClass.Warlock },
            {11, GameClass.Wizard },
        };

        private static readonly Dictionary<int, string> _primaryRaces = new Dictionary<int, string>()
        {
            {0, Race.Aarakocra},
            {1, Race.Dragonborn},
            {2, Race.Dwarf },
            {3, Race.Elf },
            {4, Race.Genasi },
            {5, Race.Gnome },
            {6, Race.Goliath },
            {7, Race.HalfElf },
            {8, Race.HalfOrc },
            {9, Race.Halfling },
            {10, Race.Human },
            {11, Race.Tiefling },
        };


        public static string PickAbility()
        {
            return SelectAbility();
        }

        public static CharacterBackground PickBackground()
        {
            return SelectBackground();
        }

        public static CharacterClass PickClass()
        {
            return SelectClass();
        }

        public static CharacterRace PickRace()
        {
            return SelectRace();
        }

        public static int RollDie(int sides)
        {
            return _rand.Next(1, sides + 1);
        }
        
        private static string SelectAbility()
        {
            return _abilities.GetRandomItem().Value;
        }

        private static CharacterBackground SelectBackground()
        {
            var backgroundName = _backgrounds.GetRandomItem().Value;
            return CharacterBackgroundFactory.CreateBackground(backgroundName);
        }

        private static CharacterClass SelectClass()
        {
            var className = _classes.GetRandomItem().Value;
            return CharacterClassFactory.CreateClass(className);
        }

        private static CharacterRace SelectRace()
        {
            var raceName = _primaryRaces.GetRandomItem().Value;
            return CharacterRaceFactory.CreateRace(raceName);
        }
    }
}
