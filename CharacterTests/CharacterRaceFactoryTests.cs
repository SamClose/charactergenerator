﻿using CharacterGenerator.Constants;
using CharacterGenerator.Factories;
using CharacterGenerator.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CharacterTests
{
    [TestFixture]
    public class CharacterRaceFactoryTests
    {
        [Test]
        public void String_Aarakocra_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.Aarakocra);
            if (race is Aarakocra)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public void String_Dragonborn_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.Dragonborn);
            if (race is Dragonborn)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public void String_Dwarf_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.Dwarf);
            if (race is Dwarf)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public void String_Elf_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.Elf);
            if (race is Elf)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public void String_Genasi_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.Genasi);
            if (race is Genasi)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }
        
        [Test]
        public void String_Gnome_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.Gnome);
            if (race is Gnome)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public void String_Goliath_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.Goliath);
            if (race is Goliath)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }


        [Test]
        public void String_HalfElf_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.HalfElf);
            if (race is HalfElf)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public void String_HalfOrc_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.HalfOrc);
            if (race is HalfOrc)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public void String_Halfling_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.Halfling);
            if (race is Halfling)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public void String_Human_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.Human);
            if (race is Human)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public void String_Tiefling_ReturnsRace()
        {
            var race = CharacterRaceFactory.CreateRace(Race.Tiefling);
            if (race is Tiefling)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }
    }
}
