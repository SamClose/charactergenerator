using CharacterGenerator.Models;
using NUnit.Framework;

namespace CharacterTests
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            _character = new Character();
        }

        Character _character;

        [Test]
        public void NewCharacter_CharacterAbilitiesInExpectedRange_True()
        {
            var minVal = 3; //3 before mods ...nothing reduces, does it?
            var maxVal = 20; //18 before mods ...+2?
            if (_IsValueOutsideRange(minVal, maxVal, _character.Strength ?? 0))
            {
                Assert.Fail();
            }
            else if(_IsValueOutsideRange(minVal, maxVal, _character.Constitution ?? 0))
            {
                Assert.Fail();
            }
            else if (_IsValueOutsideRange(minVal, maxVal, _character.Dexterity ?? 0))
            {
                Assert.Fail();
            }
            else if (_IsValueOutsideRange(minVal, maxVal, _character.Wisdom ?? 0))
            {
                Assert.Fail();
            }
            else if (_IsValueOutsideRange(minVal, maxVal, _character.Intelligence ?? 0))
            {
                Assert.Fail();
            }
            else if (_IsValueOutsideRange(minVal, maxVal, _character.Charisma ?? 0))
            {
                Assert.Fail();
            }
            else
            {
                Assert.Pass();
            }
        }

        [Test]
        public void NewCharacter_ModifiersMatchAilities()
        {
            if(!_DoValuesMatch(_character.Mod_CON, _ExpectedModifier(_character.Constitution.Value)))
            {
                Assert.Fail($"Input: {_character.Constitution.Value}, output: {_character.Mod_CON}, expected: {_ExpectedModifier(_character.Constitution.Value)}");
            }
            else if (!_DoValuesMatch(_character.Mod_STR, _ExpectedModifier(_character.Strength.Value)))
            {
                Assert.Fail($"Input: {_character.Strength.Value}, output: {_character.Mod_STR}, expected: {_ExpectedModifier(_character.Strength.Value)}");
            }
            else if (!_DoValuesMatch(_character.Mod_DEX, _ExpectedModifier(_character.Dexterity.Value)))
            {
                Assert.Fail($"Input: {_character.Dexterity.Value}, output: {_character.Mod_DEX}, expected: {_ExpectedModifier(_character.Dexterity.Value)}");
            }
            else if (!_DoValuesMatch(_character.Mod_WIS, _ExpectedModifier(_character.Wisdom.Value)))
            {
                Assert.Fail($"Input: {_character.Wisdom.Value}, output: {_character.Mod_WIS}, expected: {_ExpectedModifier(_character.Wisdom.Value)}");
            }
            else if (!_DoValuesMatch(_character.Mod_INT, _ExpectedModifier(_character.Intelligence.Value)))
            {
                Assert.Fail($"Input: {_character.Intelligence.Value}, output: {_character.Mod_INT}, expected: {_ExpectedModifier(_character.Intelligence.Value)}");
            }
            else if (!_DoValuesMatch(_character.Mod_CHA, _ExpectedModifier(_character.Charisma.Value)))
            {
                Assert.Fail($"Input: {_character.Charisma.Value}, output: {_character.Mod_CHA}, expected: {_ExpectedModifier(_character.Charisma.Value)}");
            }

            Assert.Pass();
        }

        [Test]
        public void NewCharacter_HasRace_True()
        {
            if (string.IsNullOrWhiteSpace(_character.RaceName))
            {
                Assert.Fail();
            }
            else
            {
                Assert.Pass();
            }
        }

        [Test]
        public void NewCharacter_HasClass_True()
        {
            if (string.IsNullOrWhiteSpace(_character.ClassName))
            {
                Assert.Fail();
            }
            else
            {
                Assert.Pass();
            }
        }

        [Test]
        public void NewCharacter_HasSpeed_True()
        {
            if(_character.Speed > 0)
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail();
            }
        }

        [Test]
        public void NewCharacter_MaxHitPointsMatchesClassWithModifiers_True()
        {
            var expectedHealth = _character.Class.HitDie + _character.Mod_CON;
            if(!(_DoValuesMatch(_character.MaxHitPoints, expectedHealth)))
            {
                Assert.Fail();
            }

            Assert.Pass();
        }

        public void NewCharacter_MaxHitPointsMatchesCurrentHitPoints_True()
        {
            if (!(_DoValuesMatch(_character.MaxHitPoints, _character.CurrentHitPoints)))
            {
                Assert.Fail();
            }

            Assert.Pass();
        }

        private bool _IsValueOutsideRange(int min, int max, int value)
        {
            return (value < min || value > max);
        }

        private bool _DoValuesMatch(int x, int y)
        {
            return x == y;
        }

        private int _ExpectedModifier(int ability)
        {
            return (ability - 10) / 2;
        }
    }
}