﻿using CharacterGenerator.Factories;
using CharacterGenerator.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CharacterTests
{
    [TestFixture]
    public class CharacterFactory_Tests
    {
        [Test]
        public void CreateCharacter_NoState_ReturnsNewCharacter()
        {
            var character = CharacterFactory.CreateCharacter();
            Assert.IsNotNull(character);
            Assert.IsTrue(character is Character);
        }
    }
}
