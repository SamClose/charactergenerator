﻿using CharacterGenerator.Constants;
using CharacterGenerator.Exceptions;
using CharacterGenerator.Factories;
using CharacterGenerator.Models;
using NUnit.Framework;

namespace CharacterTests
{
    [TestFixture]
    public class CharacterClassFactoryTests
    {
        [Test]
        public void String_Barbarian_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Barbarian);
            if(characterClass is Barbarian)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }
        [Test]
        public void String_Bard_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Bard);
            if (characterClass is Bard)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }
        [Test]
        public void String_Cleric_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Cleric);
            if (characterClass is Cleric)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public void String_Druid_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Druid);
            if (characterClass is Druid)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public void String_Fighter_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Fighter);
            if (characterClass is Fighter)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public void String_Monk_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Monk);
            if (characterClass is Monk)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public void String_Paladin_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Paladin);
            if (characterClass is Paladin)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public void String_Ranger_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Ranger);
            if (characterClass is Ranger)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public void String_Rogue_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Rogue);
            if (characterClass is Rogue)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public void String_Sorcerer_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Sorcerer);
            if (characterClass is Sorcerer)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public void String_Warlock_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Warlock);
            if (characterClass is Warlock)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public void String_Wizard_CreatesClass()
        {
            var characterClass = CharacterClassFactory.CreateClass(GameClass.Wizard);
            if (characterClass is Wizard)
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public void UnknownClass_ThrowsException_StringNotFound()
        {
            try
            {
                var characterClass = CharacterClassFactory.CreateClass("NotAClass");
            }
            catch (StringNotFoundException)
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

    }
}
